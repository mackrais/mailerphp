<?php
/**
 * Created by PhpStorm.
 * User: MackRais
 * Date: 2019-05-15
 * Site: https://mackrais.com
 * Email: developer@mackais.com
 *
 * PHP Version: 7.3
 * Tag: config
 */

define('ROOT', __DIR__);
define('LOG_PATH', __DIR__ . '/log' . DIRECTORY_SEPARATOR);
define('DEBUG', true);