<?php
/**
 * Created by PhpStorm.
 * User: MackRais
 * Date: 2019-05-15
 * Site: https://mackrais.com
 * Email: developer@mackais.com
 *
 * PHP Version: 7.3
 * Tag:components
 */

namespace components;

use helpers\ArrayHelper;
use helpers\FileHelper;

class Mailer
{
    /**
     * If $debugMode == true, than mails will save to log file
     * @var bool
     */
    protected $debugMode;

    /**
     * Email to whom to send
     * @var string
     */
    protected $to;

    /**
     * Email from to send
     * @var string
     */
    protected $from;

    /**
     * Subject mail
     * @var string
     */
    protected $subject;

    /**
     * Content mail. Text or Html
     * @var string
     */
    protected $content;

    /**
     * Set charset to content.
     * @var string
     */
    protected $charset = 'UTF-8';

    /**
     * Array with params. Can be both one-dimensional and multidimensional.
     * Before preparing template all params will converted to one-dimensional associative array.
     * @see Mailer::replacementParams()
     * @see ArrayHelper::arrayTreeToAssociative()
     * @var array
     */
    protected $params = [];

    /**
     * Headers for mail function.
     * @var array
     */
    protected $headers = [];

    /**
     * Full log path.
     * @var string
     */
    protected $logPath = ROOT . '/log/mails/';

    /**
     * Mailer constructor.
     * @param bool $debugMode
     */
    public function __construct(bool $debugMode = false)
    {
        $this->debugMode = $debugMode;
    }

    /**
     * Set email to
     * @param string $to
     * @return self
     */
    public function setTo(string $to): self
    {
        $this->to = $to;
        return $this;
    }

    /**
     * Set email from
     * @param string $from
     * @return self
     */
    public function setFrom(string $from): self
    {
        $this->from = $from;
        return $this;
    }

    /**
     * Set subject for email
     * @param string $subject
     * @return self
     */
    public function setSubject(string $subject): self
    {
        $this->subject = $subject;
        return $this;
    }

    /**
     * Set text body for email
     * @param string $content
     * @return self
     */
    public function setTextBody(string $content): self
    {
        $this->content = $content;
        $this->headers['Content-Type'] = 'text/plain';
        return $this;
    }

    /**
     * Set html body for mail
     * @param string $content
     * @return self
     */
    public function setHtmlBody(string $content): self
    {
        $this->headers['Content-Type'] = 'text/html';
        $this->content = $content;
        return $this;
    }

    /**
     * Set dynamic params for template mail
     * Before preparing template all params will converted to one-dimensional associative array.
     * @see Mailer::replacementParams()
     * @see ArrayHelper::arrayTreeToAssociative()
     * @var array
     * @return self
     */
    public function setParams(array $params): self
    {
        $this->params = $params;
        return $this;
    }

    /**
     * Main method to send mail.
     * If $debugMode == true, than mails will save to log file
     * @return bool
     */
    public function send(): bool
    {
        if ($this->debugMode) {
            return $this->saveToLog();
        } else {
            $headers = $this->prepareHeaders();
            $content = $this->replacementParams($this->params, $this->content);
            $subject = $this->replacementParams($this->params, $this->subject);
            return mail($this->to, $subject, $content, $headers);
        }
    }

    /**
     * List with default headers
     * @return array
     */
    protected function defaultHeaders(): array
    {
        return [
            "MIME-Version" => "1.0",
            'X-Mailer' => 'PHP/' . phpversion(),
            'From' => $this->from,
        ];
    }

    /**
     * Preparing header for mail function
     * @return array
     */
    protected function prepareHeaders(): array
    {
        if (is_array($this->headers) && !empty($this->headers)) {
            $defaultHeaders = $this->defaultHeaders();
            $headersList = array_merge($this->headers, $defaultHeaders);
            $headersList['Content-Type'] .= '; charset=' . $this->charset;
            return $headersList;
        }
        return [];
    }

    /**
     *
     * !!! Warning all parameters normalize by method @see ArrayHelper::arrayTreeToAssociative()
     *
     * Replacement params in template
     *
     * for example
     * $params = [ 'user' => ['name' => 'Bob', 'age' => 22], 'date' => '2017-10-11']
     * $template = 'Hello {{user.name}} your age {{user.age}}'
     *
     * Output : 'Hello Bob your age 22'
     *
     * @param array $params
     * @param string $template
     *
     * @return string
     */
    protected function replacementParams(array $params, string $template): string
    {
        $normalizeArray = [];
        ArrayHelper::arrayTreeToAssociative($params, $normalizeArray, '.');
        $keys = array_map(function ($item) {
            return '{{' . $item . '}}';
        }, array_keys($normalizeArray));
        return (string)str_replace($keys, array_values($normalizeArray), $template);
    }

    /**
     * Save to log file for debug mode
     *
     * @return bool
     */
    protected function saveToLog(): bool
    {
        $headers = $this->prepareHeaders();
        $content = $this->replacementParams($this->params, $this->content);
        $subject = $this->replacementParams($this->params, $this->subject);
        $log = "Headers: " . var_export($headers, true) . PHP_EOL;
        $log .= "Params: " . var_export($this->params, true) . PHP_EOL;
        $log .= "From: " . $this->from . PHP_EOL;
        $log .= "To: " . $this->to . PHP_EOL;
        $log .= "Original Subject: " . $this->subject . PHP_EOL;
        $log .= "Replacement Subject: " . $subject . PHP_EOL;
        $log .= "Original Content: " . $this->content . PHP_EOL;
        $log .= "Replacement Content: " . $content . PHP_EOL;
        return FileHelper::writeToFile($log, $this->logPath, 'email_' . rand(100, 999) . '_' . date('Y-m-d_H:i:s') . '.eml');
    }
}
