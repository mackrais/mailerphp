<?php
/**
 * Created by PhpStorm.
 * User: MackRais
 * Date: 2019-05-15
 * Site: https://mackrais.com
 * Email: developer@mackais.com
 *
 * PHP Version: 7.3
 * Tag:helper
 */

namespace helpers;

class FileHelper
{
    /**
     * Create directories
     *
     * @param      $path
     * @param int $mode
     * @param bool $recursive
     *
     * @return bool
     */
    public static function createDir($path, $mode = 0775, $recursive = true): bool
    {
        if (is_dir($path)) {
            return true;
        }
        $parentDir = dirname($path);
        if ($recursive && !is_dir($parentDir)) {
            static::createDir($parentDir, $mode, true);
        }
        $result = false;
        if (is_writable($parentDir)) {
            $result = mkdir($path, $mode);
            chmod($path, $mode);
        }
        return $result;
    }

    /**
     * Make and write data to file
     *
     * @param        $data
     * @param string $pathFile
     * @param string $nameFile
     * @param string $modeFile
     * @return bool
     */
    public static function writeToFile($data, $pathFile, $nameFile = 'file.txt', $modeFile = 'w') : bool
    {
        ob_start();
        print_r($data);
        $output = ob_get_clean();
        $pathFile = trim($pathFile);
        if (!is_dir($pathFile)) {
            static::createDir($pathFile, 0775, true);
        }
        $pathFile = $pathFile . $nameFile;
        $filehandle = fopen($pathFile, $modeFile);
        $result = fwrite($filehandle, $output);
        fclose($filehandle);
        return (bool)$result;
    }
}