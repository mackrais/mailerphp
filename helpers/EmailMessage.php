<?php
/**
 * Created by PhpStorm.
 * User: MackRais
 * Date: 2019-05-15
 * Site: https://mackrais.com
 * Email: developer@mackais.com
 *
 * PHP Version: 7.3
 * Tag:helper
 */

namespace helpers;

use components\Mailer;

class EmailMessage
{
    /**
     * Send `welcome` latter
     *
     * @param string $to
     * @return bool
     */
    public static function sendWelcome(string $to): bool
    {
        $mailer = new Mailer(DEBUG);
        try {
            return $mailer
                ->setTo($to)
                ->setFrom('infor@forbytes.com')
                ->setSubject('Welcome as a new customer')
                ->setParams(['email' => $to])
                ->setHtmlBody('Hi {{email}} <br>We would like to welcome you as customer on our site!<br><br>Best Regards,<br>Forbytes Team')
                ->send();
        } catch (\Throwable $e) {
            static::saveLog($e, "\n\nAn error occurrence to send email welcome to email: {$to} \r\n");
            return false;
        }
    }

    /**
     * Send `come back` latter
     *
     * @param string $to
     * @param string $voucher
     * @return bool
     */
    public static function sendComeBack(string $to, string $voucher): bool
    {
        $mailer = new Mailer(DEBUG);
        try {
            return $mailer
                ->setTo($to)
                ->setFrom('infor@forbytes.com')
                ->setSubject('We miss you as a customer')
                ->setParams(['email' => $to, 'voucher' => $voucher])
                ->setHtmlBody("Hi {{email}}<br>We miss you as a customer. Our shop is filled with nice products. Here is a voucher that gives you 50 kr to shop for.<br>Voucher: {{voucher}}<br><br>Best Regards,<br>Forbytes Team")
                ->send();
        } catch (\Throwable $e) {
            static::saveLog($e, "\n\nAn error occurrence to send email come back to email: {$to} \r\n");
            return false;
        }
    }

    /**
     * Write data to log file
     *
     * @param \Throwable $e
     * @param string $message
     */
    protected static function saveLog(\Throwable $e, string $message = '')
    {
        $message .= $e->getMessage() . PHP_EOL . $e->getTraceAsString();
        FileHelper::writeToFile($message, LOG_PATH, 'mail.error.log', 'a');
    }
}