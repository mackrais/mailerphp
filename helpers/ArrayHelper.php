<?php
/**
 * Created by PhpStorm.
 * User: MackRais
 * Date: 2019-05-15
 * Site: https://mackrais.com
 * Email: developer@mackais.com
 *
 * PHP Version: 7.3
 * Tag: helper
 */

namespace helpers;

class ArrayHelper
{
    /**
     * Convert array from tree structure to associative array
     *
     * @example:
     *             $myArray = [
     *                           '1',
     *                            'key' => 'value',
     *                            ['item_in_array'],
     *                            [
     *                            'has_children' => [
     *                                'first' => [
     *                                    'key_1_1' => 'value_1_1'
     *                                ]
     *                            ]
     *                       ]
     *                    ];
     *
     *         $normalizeArray = [];
     *         ArrayHelper::arrayTreeToAssociative($myArray, $normalizeArray, '.');
     *
     *         // Result:
     *
     *         Array
     *          (
     *               [0] => 1
     *               [key] => value
     *               [1.0] => item_in_array
     *               [2.has_children.first.key_1_1] => value_1_1
     *          )
     *
     *
     * @param array  $input
     * @param array  $output
     * @param string $glueSymbol
     * @param string $_index
     * @param null   $groupKey
     */
    static function arrayTreeToAssociative(
        array $input,
        array &$output = [],
        string $glueSymbol = '.',
        &$_index = '',
        $groupKey = null
    )
    {
        foreach ($input as $key => $data) {
            $_index .= $key . $glueSymbol;
            if (is_array($data)) {
                static::arrayTreeToAssociative(
                    $data,
                    $output,
                    $glueSymbol,
                    $_index,
                    sizeof($data) > 1 ? $key : null
                );
            } else {
                if ($groupKey !== null && rtrim($_index, $glueSymbol) !== $groupKey . $glueSymbol . $key) {
                    $output[$groupKey . $glueSymbol . rtrim($_index, $glueSymbol)] = $data;
                } else {
                    $output[rtrim($_index, $glueSymbol)] = $data;
                }
                $_index = '';
            }
        }
    }
}