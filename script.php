<?php
/**
 * Created by PhpStorm.
 * User: MackRais
 * Date: 2019-05-15
 * Site: https://mackrais.com
 * Email: developer@mackais.com
 *
 * PHP Version: 7.3
 * Tag:console
 */

require ('config.php');
require('autoloader.php');

Autoloader::register();

use helpers\EmailMessage;
use models\DataLayer;

$voucher = "X32Sf202DV";
$listCustomers = DataLayer::ListCustomers();
$listOrders = DataLayer::ListOrders();
$orderEmails = array_column($listOrders, 'customerEmail');
unset($listOrders);

$dtYesterday = (new \DateTime())->modify('-1 day');
foreach ($listCustomers as $customer) {
    if (!in_array($customer->email, $orderEmails)) {
        $result = EmailMessage::sendComeBack($customer->email, $voucher);
        if($result){
            echo "Successfully send latter `come back` to {$customer->email}\r\n";
        }else{
            echo "Error send latter `come back` to {$customer->email}: See logs " . LOG_PATH . "\r\n";
        }
    }
    if ($customer->createdAt > $dtYesterday) {
        $result = EmailMessage::sendWelcome($customer->email);
        if($result){
            echo "Successfully send latter `welcome` to {$customer->email}\r\n";
        }else{
            echo "Error send latter `welcome` to {$customer->email}: See logs " . LOG_PATH . "\r\n";
        }
    }
}
